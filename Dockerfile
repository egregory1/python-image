FROM python

USER root

COPY requirements.txt .
RUN pip install -r requirements.txt
